object Testing {

  def Testing (number : Int, values : (Int => Boolean)*) : Boolean = {
    values.forall(
      (p: (Int) => Boolean) => p(number)
    )
  }

  def test1 (temp : Int) : Boolean = {
    temp >= 3
  }

  def test2 (temp : Int) : Boolean = {
    temp >= 4
  }

  def test3 (temp : Int) : Boolean = {
    temp >= 5
  }

  def test4 (temp : Int) : Boolean = {
    temp >= 6
  }

  def test5 (temp : String) : Boolean = {
    temp.length >= 7
  }

}
