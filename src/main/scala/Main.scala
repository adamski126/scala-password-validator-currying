object Main {
  def main(args: Array[String]): Unit = {
    val pass = "ab34Ee"


    println(Password.Password(
      pass,
      Password.minLen(3),
      Password.maxLen(7),
      Password.oneCapitalLetter,
      Password.oneSmallLetter,
      Password.oneDigit,
      Password.twoDigits
    ))

//    val num = 7

//    println(Testing.Testing(
//      num,
//      Testing.test1,
//      Testing.test2,
//      Testing.test3,
//      Testing.test4
//    ))

  }
}
