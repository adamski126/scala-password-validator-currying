object Password {

  def Password (pass : String, vals : ((String) => Boolean)*) : Boolean = {
      vals.forall(
        (f: ((String) => Boolean)) => f (pass)
      )
  }

  def minLen (min_len : Int)(pass : String)  : Boolean = {
    pass.length >= min_len
  }

  def maxLen (max_len : Int)(pass : String)  : Boolean = {
    pass.length <= max_len
  }

  def oneCapitalLetter (pass : String) : Boolean = {
    pass.matches(".*[A-Z].+")
  }

  def oneSmallLetter (pass : String) : Boolean = {
    pass.matches(".*[a-z].+")
  }

  def oneDigit (pass : String) : Boolean = {
    pass.matches(".*\\d.+")
  }

  def twoDigits (pass : String) : Boolean = {
    pass.matches(".*\\d.*\\d.+")
  }

}
